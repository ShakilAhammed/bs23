using System;
using System.Threading.Tasks;
using SocialApp.Domain;

namespace SocialApp.Application {
    public sealed class CommentAlterCommandPolicy : IPolicy<CommentAlterCommand> {
        #region Fields
        private ICommentService commentService;
        private IRoleService roleService;
        #endregion

        #region Constructor(s)
        public CommentAlterCommandPolicy(ICommentService commentService, IRoleService roleService) {
            this.commentService = commentService;
            this.roleService = roleService;
        }
        #endregion

        public async Task<PolicyResult> Authorize(CommentAlterCommand action) {
            // Check if user owns comment
            if (await commentService.IsOwner(action.CommentId, action.User.Username)) {
                return PolicyResult.Authorized();
            }

            // Is the user an admin?
            if (await roleService.IsUserAdmin(action.User.Username)) {
                return PolicyResult.Authorized();
            }

            // Is the user a moderator?

           

            if (await roleService.IsUserModerator(action.User.Username)) {
                return PolicyResult.Authorized();
            }

            return PolicyResult.Unauthorized();
        }
    }
}