using SocialApp.Domain;
using SocialApp.Domain.Paging;

namespace SocialApp.Application {
    public sealed class CommentFindByUserQuery : AnonymousQuery, IPagableQuery {
        #region Properties
        public string Username { get; }
        public PaginationInfo Paging { get; }
        #endregion

        #region Constructor(s)
        public CommentFindByUserQuery(string username, PaginationInfo? paging = null, User? user = null) : base(user) {
            Username = username;
            Paging = paging ?? new PaginationInfo(0, 25);
        }
        #endregion
    }
}