using SocialApp.Domain;
using SocialApp.Domain.Paging;

namespace SocialApp.Application {
    public sealed class PostFindByNewQuery : AnonymousQuery, IPagableQuery {
        #region Properties
        public PaginationInfo Paging { get; }
        #endregion

        #region Constructor(s)
        public PostFindByNewQuery(PaginationInfo? paging = null, User? user = null) : base(user) {
            Paging = paging ?? new PaginationInfo(0, 25);
        }
        #endregion
    }
}