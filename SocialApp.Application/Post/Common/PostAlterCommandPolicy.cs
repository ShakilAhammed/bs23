using System;
using System.Threading.Tasks;
using SocialApp.Domain;

namespace SocialApp.Application {
    public sealed class PostAlterCommandPolicy : IPolicy<PostAlterCommand> {
        #region Fields
        private IPostService postService;
        private IRoleService roleService;
        #endregion

        #region Constructor(s)
        public PostAlterCommandPolicy(IPostService postService, IRoleService roleService) {
            this.postService = postService;
            this.roleService = roleService;
        }
        #endregion

        public async Task<PolicyResult> Authorize(PostAlterCommand action) {
            // Check if user owns Post
            if (await postService.IsOwner(action.PostId, action.User.Username)) {
                return PolicyResult.Authorized();
            }

            // Is the user an admin?
            if (await roleService.IsUserAdmin(action.User.Username)) {
                return PolicyResult.Authorized();
            }

           

            if (await roleService.IsUserModerator(action.User.Username)) {
                return PolicyResult.Authorized();
            }

            return PolicyResult.Unauthorized();
        }
    }
}