using SocialApp.Domain;
using FluentValidation;
using SocialApp.Application;

namespace SocialApp.Application {
    /// <summary>
    /// Validator to validate that the update parameters of a post update are okay.
    /// </summary>
    public sealed class PostUpdateCommandValidator : FluentValidatorAdapter<PostUpdateCommand> {
        public PostUpdateCommandValidator() {
            RuleFor(p => p.User).NotNull().WithMessage("User performing the action is null.");

            RuleFor(p => p.PostId).GreaterThan(0).WithMessage("Id to update is required.");

            RuleFor(p => p.Update.Body).NotNull().WithMessage("Body is required");
            RuleFor(p => p.Update.Body).NotEmpty().WithMessage("Body is required.");
            RuleFor(p => p.Update.Body).MaximumLength(Constant.BodyMaxLength).WithMessage($"Body must be {Constant.BodyMaxLength} characters or less.");
        }
    }
}