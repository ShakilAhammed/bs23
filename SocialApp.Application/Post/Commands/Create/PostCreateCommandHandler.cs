using System;
using System.Threading.Tasks;
using SocialApp.Domain;

namespace SocialApp.Application {
    public sealed class PostCreateCommandHandler : CommandHandler<PostCreateCommand> {
        #region Fields
        private IPostService postService;
        #endregion

        #region Constructor(s)
        public PostCreateCommandHandler(IPostService postService) {
            this.postService = postService;
        }
        #endregion

        #region Publics
        [Validate(typeof(PostCreateCommandValidator))]
        protected async override Task<Either<CommandResult, Error>> ExecuteCommand(PostCreateCommand command) {

            

            Post p = await postService.Create(command.Data, command.User);
            return Insert(p.Id);
        }
        #endregion
    }
}