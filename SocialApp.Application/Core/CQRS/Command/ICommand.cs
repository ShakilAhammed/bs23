using SocialApp.Domain;

namespace SocialApp.Application {
    /// <summary>
    /// Interface for commands to implement.
    /// </summary>
    public interface ICommand : IAction { }
}