using SocialApp.Domain;

namespace SocialApp.Application {
    /// <summary>
    /// Marker interface for query parameters.
    /// </summary>
    public interface IQuery : IAction { }
}