using SocialApp.Domain.Paging;

namespace SocialApp.Application {
    public interface IPagableQuery {
        #region Properties
        PaginationInfo Paging { get; }
        #endregion
    }
}