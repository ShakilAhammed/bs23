using SocialApp.Domain;

namespace SocialApp.Application {
    public sealed class AuthorizationError : Error {
        #region Properties
        public AuthorizationError(PolicyResult result) : base(result.Failure ?? "Unauthorized") { }
        #endregion
    }
}