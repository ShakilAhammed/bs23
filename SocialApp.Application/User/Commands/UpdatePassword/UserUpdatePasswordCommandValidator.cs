using FluentValidation;
using SocialApp.Domain;
using SocialApp.Application;

namespace SocialApp.Application {
    /// <summary>
    /// Validator to validate that a password is safe to update.
    /// </summary>
    public sealed class UserUpdatePasswordCommandValidator : FluentValidatorAdapter<UserUpdatePasswordCommand> {
        public UserUpdatePasswordCommandValidator() {
            RuleFor(c => c.User).NotNull().WithMessage("User is required.");

            RuleFor(c => c.UpdatePassword.CurrentPassword).NotNull().WithMessage("Current password is required.");
            RuleFor(c => c.UpdatePassword.CurrentPassword).NotEmpty().WithMessage("Current password is required.");

            RuleFor(c => c.UpdatePassword.NewPassword).NotNull().WithMessage("Password is required.");
            RuleFor(c => c.UpdatePassword.NewPassword).NotEmpty().WithMessage("Password is required.");
            RuleFor(c => c.UpdatePassword.NewPassword).MinimumLength(Constant.PasswordMinLength).WithMessage($"Password must be at least {Constant.PasswordMinLength} characters.");
        }
    }
}