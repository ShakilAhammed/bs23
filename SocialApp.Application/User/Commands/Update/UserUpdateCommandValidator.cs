using SocialApp.Domain;
using FluentValidation;
using SocialApp.Application;

namespace SocialApp.Application {
    /// <summary>
    /// Validator to validate when a user is updated.
    /// </summary>
    public sealed class UserUpdateCommandValidator : FluentValidatorAdapter<UserUpdateCommand> {
        public UserUpdateCommandValidator() {
            RuleFor(reg => reg.User).NotNull().WithMessage("User is required.");
            RuleFor(command => command.Update.Email).EmailAddress().WithMessage("Email must be valid.");
            RuleFor(command => command.Update.Email).MaximumLength(Constant.EmailMaxLength).WithMessage($"Email must be less than {Constant.EmailMaxLength} characters.");
        }
    }
}