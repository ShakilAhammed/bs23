using System;
using SocialApp.Domain;
using FluentValidation;
using FluentValidation.Results;
using System.Linq;
using SocialApp.Application;

namespace SocialApp.Application {
    /// <summary>
    /// Validator to validate user registrations.
    /// </summary>
    public sealed class RegisterUserCommandValidator : FluentValidatorAdapter<RegisterUserCommand> {

        #region Constructor(s)
        public RegisterUserCommandValidator() {
            //Username
            RuleFor(cmd => cmd.Registration.Username).NotNull().WithMessage("Username is required.");
            RuleFor(cmd => cmd.Registration.Username).MinimumLength(Constant.UsernameMinLength).WithMessage($"Username must be at least {Constant.UsernameMinLength} characters.");
            RuleFor(cmd => cmd.Registration.Username).MaximumLength(Constant.UsernameMaxLength).WithMessage($"Username must be less than {Constant.UsernameMaxLength} characters.");
            RuleFor(cmd => cmd.Registration.Username).Matches(RegexPattern.UrlSafe).WithMessage("Username may only contain letters, numbers, underscores, or hypens.");
            RuleFor(cmd => cmd.Registration.Username).Must((username) => !User.IsUsernameBanned(username)).WithMessage("Username is unavailable.");

            // Password
            RuleFor(cmd => cmd.Registration.Password).NotNull().WithMessage("Password is required.");
            RuleFor(cmd => cmd.Registration.Password).NotEmpty().WithMessage("Password is required.");
            RuleFor(cmd => cmd.Registration.Password).MinimumLength(Constant.PasswordMinLength).WithMessage($"Password must be at least {Constant.PasswordMinLength} characters.");

            // Email is only validated when provided.
            When(cmd => !String.IsNullOrWhiteSpace(cmd.Registration.Email), () => {
                RuleFor(cmd => cmd.Registration.Email).EmailAddress().WithMessage("Email must be valid.");
                RuleFor(cmd => cmd.Registration.Email).MaximumLength(Constant.EmailMaxLength).WithMessage($"Email must be less than {Constant.EmailMaxLength} characters.");
            });
        }
        #endregion
    }
}