using SocialApp.Domain;

namespace SocialApp.Application {
    public sealed class LoginUserCommand : AnonymousCommand {
        #region Properties
        public UserCredentials Credentials { get; set; } = null!;
        #endregion
    }
}