using SocialApp.Domain;

namespace SocialApp.Domain {
    public sealed class UserLogin : Entity<UserLogin> {
        #region Properties
        public int UserId { get; }
        #endregion

        #region Constructor(s)

        internal UserLogin() { }
        internal UserLogin(int userId) {
            UserId = userId;
        }

        internal UserLogin(int id, int userId) {
            Id = id;
            UserId = userId;
        }
        #endregion
    }
}