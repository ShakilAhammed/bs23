namespace SocialApp.Domain {
    public interface IUpdatable<TUpdate> {
        void Update(TUpdate update);
    }
}