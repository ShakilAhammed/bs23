namespace SocialApp.Domain {
    public interface IDeletable {
        void Delete();
    }
}