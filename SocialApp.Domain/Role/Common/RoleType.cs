namespace SocialApp.Domain {
    public enum RoleType {
        Admin = 0,
        Moderator = 1,
    }
}