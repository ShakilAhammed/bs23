using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialApp.Domain {
    public interface IRoleRepo : IRepo<Role> {
        #region Publics
        Task<bool> IsUserAdmin(string username);
        Task<bool> IsUserModerator(string username);
        Task<Role?> FindAdminRole(User user);
        Task<Role?> FindModeratorRole(User user);
        #endregion
    }
}