using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialApp.Domain {
    public sealed class RoleService : IRoleService {
        #region Fields
        private IEventBus eventBus;
        private IRoleFactory roleFactory;
        private IUserRepo userRepo;
        private IRoleRepo roleRepo;
        #endregion

        #region Constructor(s)
        public RoleService(IEventBus eventBus, IRoleFactory roleFactory, IUserRepo userRepo, IRoleRepo roleRepo) {
            this.eventBus = eventBus;
            this.roleFactory = roleFactory;
            this.userRepo = userRepo;
            this.roleRepo = roleRepo;
        }
        #endregion

        #region Publics
        public async Task<bool> IsUserAdmin(string username) => await roleRepo.IsUserAdmin(username);
        public async Task<bool> IsUserModerator(string username) => await roleRepo.IsUserModerator(username);

        public async Task AddAdmin(string username, User user) {
            User newAdmin = await GetUserOrThrow(username);

            Role adminRole = roleFactory.CreateAdminRole(newAdmin);
            await roleRepo.Add(adminRole);

            await eventBus.Dispatch(new AdminAddedEvent(newAdmin));
        }

        public async Task AddModeratorToSpace(string username, string spaceName, User user) {
            User newMod = await GetUserOrThrow(username);

            Role modRole = roleFactory.CreateModeratorRole(newMod);
            await roleRepo.Add(modRole);

            await eventBus.Dispatch(new ModeratorAddedToSpaceEvent(newMod));
        }

        public async Task RemoveAdmin(string username, User user) {
            User oldAdmin = await GetUserOrThrow(username);

            Role? adminRole = await roleRepo.FindAdminRole(oldAdmin);

            if (adminRole == null) {
                throw new InvalidOperationException($"User {oldAdmin.Username} was not an admin");
            }

            await roleRepo.Delete(adminRole);
            await eventBus.Dispatch(new AdminRemovedEvent(oldAdmin));
        }

        public async Task RemoveModeratorFromSpace(string username, string spaceName, User user) {
            User oldMod = await GetUserOrThrow(username);

            Role? modRole = await roleRepo.FindModeratorRole(user);

           

            await roleRepo.Delete(modRole);
            await eventBus.Dispatch(new ModeratorRemovedFromSpaceEvent( user));
        }
        #endregion

        #region Privates
        private async Task<User> GetUserOrThrow(string username) =>
            await userRepo.FindByUsername(username) ?? throw new NotFoundException($"User {username} was not found");

        #endregion
    }
}