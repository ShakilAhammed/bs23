namespace SocialApp.Domain {
    public sealed class ModeratorRemovedFromSpaceEvent : IDomainEvent {
        #region Properties
        public User User { get; }
        #endregion

        #region Constructor(s)
        public ModeratorRemovedFromSpaceEvent(User user) {
            User = user;
        }
        #endregion
    }
}