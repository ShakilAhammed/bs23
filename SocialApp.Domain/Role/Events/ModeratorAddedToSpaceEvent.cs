namespace SocialApp.Domain {
    public sealed class ModeratorAddedToSpaceEvent : IDomainEvent {
        #region Properties
        public User User { get; }
        #endregion

        #region Constructor(s)
        public ModeratorAddedToSpaceEvent(User user) {
            User = user;
        }
        #endregion
    }
}