﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocialApp.Domain
{
    public class Constant
    {
        public static string SiteWideDomain = "";
        public int PageSize = 25;

        public static int BodyMaxLength { get; set; }
        public static int TitleMaxLength { get; set; }
        public static int EmailMaxLength { get; set; }
        public static int PasswordMinLength { get; set; }
        public static int UsernameMaxLength { get; set; }
        public static int UsernameMinLength { get; set; }
    }
}
