using System.Collections.Generic;
using System.Threading.Tasks;
using SocialApp.Domain.Paging;
using SocialApp.Domain;

namespace SocialApp.Domain {
    /// <summary>
    /// Marker interface for now.
    /// </summary>
    public interface ICommentRepo : IRepo<Comment> {
        Task<bool> IsOwner(int commentId, string username);

        Task<bool> Exists(int commentId);
    }
}