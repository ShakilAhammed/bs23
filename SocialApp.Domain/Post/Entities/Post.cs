using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace SocialApp.Domain {
    /// <summary>
    /// A post of the website.
    /// </summary>
    public sealed class Post : Entity<Post>, IUserEntity, IVotableEntity, IAuditableEntity, IUpdatable<PostUpdate>, IDeletable {

        #region Properties
        public int UserId { get; }
        public VoteStats Votes { get; set; } = new VoteStats();
        VotableEntityType IVotableEntity.VotableEntityType => VotableEntityType.Post;
        public PostType Type { get; }
        public string Title { get; }
        public string Body { get; private set; } = "";
        public DateTime CreationDate { get; set; } = DateTime.UtcNow;
        public bool WasUpdated { get; private set; }
        public bool WasDeleted { get; private set; }
        public int CommentCount { get; set; }
        public ICollection<Comment> Comments { get; set; }
        #endregion

        #region Constructor(s)
        internal Post() { }
        internal Post(PostCreate createData, User user) {
            Type = createData.Type;
            Title = createData.Title;
            Body = createData.Body;
            UserId = user.Id;

            if (Type == PostType.Link && !Regex.IsMatch(Body, RegexPattern.UrlProtocol)) {
                Body = $"http://{Body}";
            }
        }

        internal Post(int id, int userId, int spaceId, PostType type, string title, string body, DateTime creationDate, int commentCount, VoteStats votes, bool wasUpdated = false, bool wasDeleted = false) {
            Id = id;
            UserId = userId;
            Type = type;
            Title = title;
            Body = body;
            CreationDate = creationDate;
            CommentCount = commentCount;
            Votes = votes;
            WasUpdated = wasUpdated;
            WasDeleted = wasDeleted;

            if (Type == PostType.Link && !Regex.IsMatch(Body, RegexPattern.UrlProtocol)) {
                Body = $"http://{Body}";
            }
        }
        #endregion

        #region Publics
        public void Update(PostUpdate update) {
            if (Type == PostType.Link) {
                throw new InvalidOperationException();
            }

            if (WasDeleted) {
                throw new InvalidOperationException("Post was already deleted");
            }

            WasUpdated = true;
            Body = update.Body;
        }

        public void Delete() {
            if (WasDeleted) {
                throw new InvalidOperationException();
            }

            Body = "[deleted]";
            WasDeleted = true;
        }
        #endregion
    }
}