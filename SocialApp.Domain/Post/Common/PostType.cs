
namespace SocialApp.Domain {
    public enum PostType : byte {
        Link = 0,
        Text = 1,
    }
}