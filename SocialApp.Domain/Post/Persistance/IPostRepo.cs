using System.Collections.Generic;
using System.Threading.Tasks;
using SocialApp.Domain;
using SocialApp.Domain.Paging;

namespace SocialApp.Domain {
    /// <summary>
    /// CRUD interface for managing pots in the database.
    /// </summary>
    public interface IPostRepo : IRepo<Post> {
        Task<bool> IsOwner(int postId, string username);
        Task<bool> Exists(int postId);
    }
}