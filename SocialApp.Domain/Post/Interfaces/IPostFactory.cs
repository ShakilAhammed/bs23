using System;
using SocialApp.Domain;

namespace SocialApp.Domain {
    public interface IPostFactory : IFactory<Post> {
        Post Create(PostCreate creationData, User user);
        Post Create(int id, int userId, int spaceId, PostType type, string title, string body, DateTime creationDate, int commentCount, int upvotes, int downvotes, bool wasUpdated, bool wasDeleted);
    }
}