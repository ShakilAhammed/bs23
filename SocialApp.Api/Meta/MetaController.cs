using Microsoft.AspNetCore.Mvc;

namespace SocialApp.Api {
    [Route("api/meta")]
    [ApiController]
    public sealed class MetaController : ApiController {
        [HttpGet("alive")]
        public ActionResult Test() {
            return Ok("All good!");
        }
    }
}