using System;

namespace SocialApp.Persistance {
    internal sealed class UserLoginRecord {
        #region Properties
        public int Id { get; set; }
        public int UserId { get; set; }
        #endregion
    }
}