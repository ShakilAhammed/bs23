﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialApp.Persistance
{
    public interface IRepository<TEntity> : IRepository
    where TEntity : class
    {
        IQueryable<TEntity> AsQueryable();

        void Add(TEntity entity);

        void Add(IEnumerable<TEntity> entities);

        TEntity GetById(object id);
        Task<TEntity> GetByIdAsync(object id);

        IQueryable<TEntity> GetAll();

        void Update(TEntity entity);

        void Update(IEnumerable<TEntity> entities);

        void Remove(Guid id);

        void Remove(int id);

        void Remove(TEntity entity);

        void Remove(params TEntity[] entities);

        void Remove(IEnumerable<TEntity> entities);
    }

    public interface IRepository : IDisposable
    {
        int SaveChanges();
    }
}
