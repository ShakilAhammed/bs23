﻿using System;

namespace SocialApp.Persistance
{
    public interface IUnitOfWork
    {
        int SaveChanges();

        void BeginTransaction();

        void CommitTransaction();

        void RollbackTransaction();

        TRepository GetRepository<TRepository>()
            where TRepository : IRepository;
    }
}
