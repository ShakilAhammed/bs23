// using SocialApp.Domain;

// namespace SocialApp.Persistance {
//     public interface IVoteReadViewMapper : IMapper<VoteRecord, VoteReadView> { }

//     public sealed class VoteReadViewMapper : IVoteReadViewMapper {
//         #region Publics
//         public VoteReadView Map(VoteRecord source) => new VoteReadView() { Direction = source.Direction };
//         #endregion
//     }
// }